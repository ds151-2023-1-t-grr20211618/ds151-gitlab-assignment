import { StatusBar } from 'expo-status-bar'
import { StyleSheet, View } from 'react-native'
import react from 'react'
import HomeScreen from './src/screens/HomeScreen'
import {AuthProvider} from './src/context/AuthContext'

//App é o componente raiz da aplicação.
//StatusBar controla o estilo da barra de status do dispositivo.
const App = () => {
  return ( <AuthProvider>
      <View style={styles.container}>
        <HomeScreen /> 
        <StatusBar style="auto" />
      </View>
    </AuthProvider > )}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'pink',
    alignItems: 'center',
    flex: 1
  },
});

export default App