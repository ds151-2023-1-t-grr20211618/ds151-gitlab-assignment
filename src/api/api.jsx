import axios from "axios";

const gitlab = axios.create({
    baseURL: 'https://gitlab.com/'
})

export default gitlab;
