import React from "react"
import AsyncStorage from '@react-native-async-storage/async-storage';

//Contexto de autenticação:
//Permite que outros componentes acessem o estado de autenticação.
const AuthContext = React.createContext()

const removeValue = async () => {
    try { await AsyncStorage.removeItem('@Access_token')
} catch (e) { console.log(e)
    }}

const authReducer = (state, action) => {
    switch (action.type) {

        case 'login': {
        return { ...state, accessToken: action.accessToken }
        }

        case 'logout': {
        removeValue()
        return { ...state, accessToken: null }
        }

        default: {
        throw new Error(`Ocorreu um erro: ${action.type}`)
        }}}

//Provedor de autenticação:
//Envolve os componentes da aplicação e fornece o contexto da autenticação p/ eles.
const AuthProvider = ({ children }) => {

//useReducer: gerencia o estado de autenticação.
const [state, dispatch] = React.useReducer(authReducer, { accessToken: null })
const value = { state, dispatch }

    React.useEffect(() => {
        if (state !== null && state !== undefined) {
        AsyncStorage.setItem('@Access_token', state)}},
        [state])

    React.useEffect(() => {
        AsyncStorage.getItem('@Access_token').then((value) => {
        if (value) {
        console.log(JSON.stringify(value))
        dispatch({ type: 'login', accessToken: value })}})},
    [])

    return (
    <AuthContext.Provider value={value}>
    {children}
    </AuthContext.Provider>)}

//Hook personalizado que retorna o contexto de autenticação:
const Authen = () => {
    const context = React.useContext(AuthContext)
    if (context === undefined) {
        throw new Error(' Authen deve ser usado dentro de um AuthProvider ')}
    return context
}

export { AuthProvider, Authen }
