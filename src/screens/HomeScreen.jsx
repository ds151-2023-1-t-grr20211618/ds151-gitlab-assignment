import React, { useEffect } from 'react';
import { Authen } from '../context/AuthContext';
import { TouchableOpacity, StyleSheet, Text, TextInput, View } from 'react-native';
import client from '../api/api';

//Tela principal da aplicação:
const HomeScreen = () => {
  const { state: { accessToken }, dispatch } = Authen();
  const [user, setUser] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [userData, setUserData] = React.useState(null);
  const [error, setError] = React.useState(null);

  //Função chamada ao clicar no botão de login:
  const tryLogin = async () => {
    try {
      const response = await client.post('oauth/token', null, {
      params: {
        grant_type: 'password',
        username: user,
        password: password,
        },
      });

      dispatch({ type: 'login', accessToken: response.data.access_token });
      setError(null);
      setUser('');
      setPassword('');
    } catch (error) {
      setError('O nome de usuário ou senha estão incorretos.');
    }
  };

  //Função chamada ao clicar no botão de logout:
  const tryLogout = () => {
    dispatch({ type: 'logout' });
  };

  //Busca os dados do usuário após o login, quando o accessToken é atualizado:
  //Se existir o accessToken, o usuário cai direto na página logada.
  useEffect(() => {
    if (accessToken) {
    fetchUserData();
    }},
    [accessToken]);

  const fetchUserData = async () => {
    try{
      const response = await client.get('user');
      setUserData(response.data);
    } catch (error) {
      console.log(error);
    }};

  if (accessToken !== null && accessToken !== undefined) {
    return (
      <View style={style2.container}>
        <Text style={style2.title}>Seja bem-vindo!</Text>
        <Text style={style2.title}>Atividade DS151 - Desenvolvimento para Dispositivos Móveis</Text>
        {userData && (
          <View style={style2.content}>
            <Text>Seja bem-vindo, {userData.name}!</Text>
            <Text>Email: {userData.email}</Text>
          </View>
        )}
        <TouchableOpacity style={style2.button} onPress={tryLogout}>
          <Text style={style2.buttonText}>Encerrar Sessão</Text>
        </TouchableOpacity>
      </View>
    );

  }else{
  return (
      <View style={style2.container}>
        <Text style={style2.title}>Fazer Login</Text>
        {error && <Text style={style2.error}>{error}</Text>}
        <View style={style2.inputContainer}>
          <TextInput
            onChangeText={setUser}
            value={user}
            placeholder="E-mail"
            style={[style2.input, style2.emailInput]}
          />
          <TextInput
            onChangeText={setPassword}
            value={password}
            placeholder="Senha"
            secureTextEntry={true}
            style={[style2.input, style2.passwordInput]}
          />
        </View>
        <TouchableOpacity style={style2.button} onPress={tryLogin}>
          <Text style={style2.buttonText}>Iniciar Sessão</Text>
        </TouchableOpacity>
      </View>
    );
  }
};

const style2 = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'pink',
    padding: 32,
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    alignSelf: 'flex-start',
    marginTop: 16,
    marginBottom: 16,
    marginLeft: 16,
    marginRight: 16
  },
  inputContainer: {
    marginBottom: 20,
    width: '200%',
  },
  input: {
    marginVertical: 8,
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderColor: '#0d6efd',
    borderWidth: 1,
    borderRadius: 30,
    width: '100%',
  },
  button: {
    backgroundColor: '#0d6efd',
    alignSelf: 'center',
    padding: 15,
    width: '70%',
    borderRadius: 12,
    marginTop: 20,
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  error: {
    color: 'red',
  },
});

export default HomeScreen;
